oracle-instantclient-basic (19.6.0.0.0-0kali4) kali-dev; urgency=medium

  * Revert "Patch binaries and libraries for libaio.so.1t64"
  * Nitpicks in control file
  * Build-Depends on Kali's fork of libaio
  * Add basic smoke test

 -- Arnaud Rebillout <arnaudr@kali.org>  Mon, 08 Apr 2024 14:09:30 +0700

oracle-instantclient-basic (19.6.0.0.0-0kali3) kali-dev; urgency=medium

  * Update dh_install in d/rules
  * Get DEB_HOST_ARCH from /usr/share/dpkg/architecture.mk
  * Reorder and beautify in debian/rules
  * Add a DESTDIR variable to make lines shorter
  * Bump debhelper compat to 13, drop dh-exec
  * Patch binaries and libraries for libaio.so.1t64

 -- Arnaud Rebillout <arnaudr@kali.org>  Thu, 04 Apr 2024 17:03:29 +0700

oracle-instantclient-basic (19.6.0.0.0-0kali2) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Update lintian override info to new format on line 2, 6.
  * Update standards version to 4.6.1, no changes needed.
  * Update lintian override info format in 
    d/oracle-instantclient-basic.lintian-overrides on line 7.
  * Update standards version to 4.6.2, no changes needed.

  [ Steev Klimaszewski ]
  * Add d/watch file to check for updates.
  * ci: Disable architectures that aren't amd64.
  * Update lintian overrides.
  * Fix lintian overrides, part 2.
  * ci: Don't build on non-amd64 arches, take 2
  * Prepare for Release

 -- Steev Klimaszewski <steev@kali.org>  Mon, 10 Apr 2023 12:59:48 -0500

oracle-instantclient-basic (19.6.0.0.0-0kali1) kali-dev; urgency=medium

  * New upstream version 19.6.0.0.0

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 06 May 2020 17:07:13 +0200

oracle-instantclient-basic (19.3.0.0.0-0kali2) kali-dev; urgency=medium

  * Fix build for i386

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 12 Dec 2019 08:46:20 +0100

oracle-instantclient-basic (19.3.0.0.0-0kali1) kali-dev; urgency=medium

  [ Sophie Brun ]
  * Update Vcs-* fields for the move to gitlab.com
  * New upstream version 19.3.0.0.0
  * Update debian/* to new version 19.3
  * Update lintian-overrides
  * Add a debian/README.source

  [ Raphaël Hertzog ]
  * Add GitLab's CI configuration file
  * Configure git-buildpackage for Kali
  * Update URL in GitLab's CI configuration file

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 09 Dec 2019 16:00:30 +0100

oracle-instantclient-basic (12.1.0.2.0-0kali2) kali-dev; urgency=medium

  * Add  missing oracle.conf for ldconfig

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 07 Mar 2019 16:24:40 +0100

oracle-instantclient-basic (12.1.0.2.0-0kali1) kali-dev; urgency=medium

  * Initial release

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 22 Feb 2019 14:53:40 +0100
